package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {

	// con este Handle nos sirve para poder servir los archivos publicos
	http.Handle("/", http.FileServer(http.Dir("./public")))
	// este HandlerFunc es para poder subir los archivos de la pag web a nuestro servidor, llama el controlador de la
	// ruta upload que se encarga de leer el archivo y guardarlo
	http.HandleFunc("/upload", upload)
	log.Println("Escuchando en http://localhost:8080")
	// se esta poniendo a escuchar el servidor (se esta corriendo)
	http.ListenAndServe(":8080", nil)

}

func upload(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		// FormFile de request, se esta recibiendo el archivo, el handle es como informacion del archivo
		file, handle, err := r.FormFile("myFile")
		if err != nil {
			log.Printf("Error al cargar archivo %v", err)
			fmt.Fprint(w, "Error al cargar el archivo %v", err)
			return
		}
		defer file.Close() //pase lo que pase se cierra el archivp

		// ReadAll se lee el archivo, se guardo en la variable data
		data, err := ioutil.ReadAll(file)
		if err != nil {
			log.Printf("Error al leer el archivo %v", err)
			fmt.Fprint(w, "Error al leer el archivo %v", err)
			return
		}

		// se escribe el archivo en el servidor, recibe la ruta en donde se va agrabar, la informacion que se va a guardar
		// y los permisos van a quedar
		err = ioutil.WriteFile("./files/"+handle.Filename, data, 0666)
		if err != nil {
			log.Printf("Error al escribir el archivo %v", err)
			fmt.Fprint(w, "Error al escribir el archivo %v", err)
			return
		}

		fmt.Fprint(w, "Cargado exitosamente")

	}
}
